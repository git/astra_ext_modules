import os, json
from scipy.io import netcdf_file

def pretty_array1d(arr_in):
    arr_str = '['
    for x in arr_in:
        arr_str += '%13.6e,' %x
    arr_str = arr_str[:-1] + ']' # Remove last comma
    return arr_str

def cdf2json(f_cdf):

    f_json = f_cdf.replace('.dat', '.json')
    cv = netcdf_file(f_cdf, 'r', mmap=False).variables

    jstr = '{\n'
#    jstr = json.dumps(json_d).replace(']},', ']},\n    ').replace('{"','{\n    "')
    for key, val in cv.items():
        jstr += '\n    "%s": { "units": "%s", "data": ' %(key, val.units.decode())
        if val.data.ndim == 1:
            jstr += pretty_array1d(val.data)
            jstr += '},\n'
        if val.data.ndim == 2:
            jstr += '[\n'
            for y in val.data:
                jstr += pretty_array1d(y)
                jstr += ',\n'
            jstr = jstr[:-2] + '\n]},\n' # Remove last comma
    jstr = jstr[:-2] + '\n\n}\n'         # Remove last comma
    with open(f_json, 'w') as fjson:
        fjson.write(jstr)
    print('Stored file %s' %f_json)
    return f_json

if __name__ == '__main__':

    ddpath = 'dd'
    onlyfiles = [f for f in os.listdir(ddpath) if os.path.isfile(os.path.join(ddpath, f))]
    for fcdf in onlyfiles:
        ext = os.path.splitext(fcdf)[-1]
        if ext == '.dat' and 'settings' not in fcdf:
            f_cdf = os.path.join(ddpath, fcdf)
            f_json = cdf2json(f_cdf)

# Check that json is formatted correctly
            with open(f_json) as fjson:
                json_d = json.load(fjson)
            if 'Ti' in json_d.keys():
                print(json_d['Ti'])
